# Installation Linux
## Sujet 
Le but de cette SAE est de créer un environnement de travail Linux pour pouvoir faire dans les meilleurs condition les exercices demandées en Bash,Python,BD etc.
## Mes Choix
Pour l'installation nous avions 3 choix différents:
- Faire un dual-boot
- Passer notre ordinateur en linux
- Créer une virtual box
- Configurer WSL2 sous windows
Personelemment j'ai choisis la 3eme option: créer une virtual box.
j'ai fais ce choix pour pouvoir utiliser de n'importe qu'elle endroit en ammenant juste ma clé
Pour ce faire j'ai installé l'aplication [virtualbox](https://www.clubic.com/telecharger-fiche30849-virtualbox.html) qui vas me servir d'hebergeur mais aussi [rufus](rufus-3.20.exe) et un dossier .iso de ubuntu pour faire de ma clé une clé bootable.
### création de la clé bootable
donc pour ce faire je me suis fourni d'une clé de 8go et d'un fichier [ubuntu-20.04.5.iso](https://www.ubuntu-fr.org/download//)
dans rufus j'ai choisis ma clés usb dans le repère *Périphérique*.
Après ça dans *type de démarrage* selectionner votre fichier .iso
Pour le Schéma de partition et le système de destination prendre GPT + UEFI (non CSM) pour les ordinateurs récents
ensuite appuyer sur demarrer et ok pour le message d'avertissement après un peu d'attente votre clé bootable a été crée
### création de la Virtual-Box
Pour commencer on rentre le nom de la machine par exemple **ubuntu Noam** et le dossier de la machine qui sera votre clée bootable
Sur l'application sélectionner *Nouvelle*. On nous demandera la mémoire. J'ai personnelement mis 2450mb pour que ma VB soit fluide je vous déconseille de descendre en dessous de 1024mb.
ensuite c'est le moment de créer votre disque dur virtuel donc le stockage alloué a linux. Encore une fois il faut mettre par rapport au stockage disponible sur votre ordinateur, j'ai mit 30go pour de n'avoir aucun problème de stockage
après cela je suis allez dans configuration/stockage j'ai mis dans IDE mon .Iso booté sur ma clé et dans stockage le stockage que je venais de créer
![image](installer-ubuntu.jpg)

appuyer sur afficher, choisissez votre langue de contenue et de clavier puis choisissez *Install ubuntu* puis *Effacer le disque et installer ubuntu*.
Pour finir j'ai créer une session appelé Noam-VirtualBox avec un mot de passe unique.
### installation des différents programme
- il  faut faire la commande **sudo snap install code --classic** pour installer visual studio code


